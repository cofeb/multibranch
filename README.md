# Setup Environments

```
export LDAP_SERVER=
export LDAP_ADMIN_DN=
export LDAP_ADMIN_PASSWORD=
export LDAP_CONFIG_PASSWORD=


export SMTP_SERVER=
export SMTP_SERVER_PORT=
export SMTP_ENCRYPTION=
export SMTP_USER=
export SMTP_PASS=
```
